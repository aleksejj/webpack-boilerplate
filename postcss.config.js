module.exports = {
    parser: 'postcss-scss',

    sourceMap: true,

    plugins: {
        'postcss-responsive-type': {}, // responsive font-size

        'postcss-preset-env': { // convert modern CSS to something most browsers can understand
            browsers: 'defaults',
            stage: 0,
            autoprefixer: {
                grid: false
            }
        },

        cssnano: { // optimize css
            preset: [
                'advanced',

                {
                    zindex: false,
                    reduceIdents: false
                }
            ]
        },

        'postcss-reporter': { // clear postcss generated reports by console.log
            clearReportedMessages: true
        }
    }
};
