window.addEventListener('resize', () => {
  document.documentElement.style.setProperty('--window-width', `${document.documentElement.clientWidth}px`);
  document.documentElement.style.setProperty('--window-height', `${window.innerHeight}px`);
});

window.dispatchEvent(new Event('resize'));
