document.body.addEventListener('keydown', (e) => {
  if (e.target.tagName.toLowerCase() === 'input' && e.target.hasAttribute('maxlength')) {
    const maxLength = parseInt(e.target.getAttribute('maxlength'), 10);
    const valueLength = e.target.value.length;

    if (valueLength > maxLength) {
      e.preventDefault();
    }
  }
});
