import '../../css/styles.scss';

const components = require.context('../../css/components/', true, /\.scss$/);
components.keys().forEach(components);

const pages = require.context('../../css/pages/', true, /\.scss$/);
pages.keys().forEach(pages);
