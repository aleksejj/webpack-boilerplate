const webpack = require('webpack');
const merge = require('webpack-merge');
const path = require('path');

const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const base = require('./base');

module.exports = merge(base, {
    mode: 'production',

    output: {
        filename: 'app.[hash:8].js',
        chunkFilename: '[name].[hash:8].chunk.js',
    },

    optimization: {
        namedModules: true,
        namedChunks: true,

        splitChunks: {
            // include all types of chunks
            chunks: 'all'
        },

        minimizer: [
            new TerserPlugin({
                cache: true,
                parallel: true,
                sourceMap: true
            }),

            new OptimizeCSSAssetsPlugin({
                cssProcessorOptions: {
                    map: {
                        inline: false,
                        annotation: true
                    }
                }
            })
        ]
    },

    module: {
        rules: [{
            test: /(\.scss)$/,
            use: [
                {
                    // extracts CSS into separate files (creates a CSS file per JS file which contains CSS)
                    loader: MiniCssExtractPlugin.loader
                },

                {
                    loader: 'css-loader', // translates CSS into CommonJS
                    options: {
                        sourceMap: true,
                        importLoaders: 2
                    }
                },

                {
                    loader: 'postcss-loader', // post processes CSS & autoprefixes
                },

                {
                    // resolves relative paths based on the original source file.
                    loader: 'resolve-url-loader',
                    options: {
                        sourceMap: true,
                    }
                },

                {
                    loader: 'sass-loader', // compiles SCSS to CSS
                    options: {
                        sourceMap: true,
                    }
                },
            ]
        }]
    },

    plugins: [
        new webpack.DefinePlugin({
            __ENV__: JSON.stringify('production'),
        }),

        new MiniCssExtractPlugin({
            filename: 'app.[hash:8].css'
        })
    ],

    devtool: 'source-map'
});
