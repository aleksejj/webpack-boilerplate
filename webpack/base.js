const path = require('path');

const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const EntrypointsPlugin = require('./EntrypointsPlugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const Dotenv = require('dotenv-webpack');
const WebpackBar = require('webpackbar');

const rootPath = path.join(__dirname, '../local/templates/main');
const entryPath = path.join(rootPath, '/js');
const outputPath = path.join(rootPath, '/assets');

process.noDeprecation = true;

module.exports = {
  context: entryPath,

  entry: [
    "./app.js"
  ],

  output: {
    path: outputPath,
    publicPath: '/local/templates/main/assets/'
  },

  module: {
    rules: [
      {
        enforce : 'pre',
        test    : /\.js$/,
        exclude : [/node_modules/],
        loader  : 'eslint-loader'
      },

      {
        test    : /\.js?$/,
        exclude : [/node_modules/],
        loader  : 'babel-loader'
      },

      {
        test: /\.(woff(2)?|eot|ttf|otf|png|jpe?g|gif|svg)$/,
        // loader: 'url-loader',
        loader: 'file-loader',
        exclude: /\/svg-icons\//,
        options: {
          // limit: 10000
          name: '[path][name].[ext]'
        }
      },

      {
        test: /\/svg-icons\//,
        loader: 'svg-sprite-loader'
      },
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      }
    ]
  },

  plugins: [
    new CleanWebpackPlugin(),
    new SpriteLoaderPlugin(),
    new EntrypointsPlugin(),
    new VueLoaderPlugin(),
    new Dotenv(),
    new WebpackBar()
  ],

  resolve: {
    extensions: ['*', '.js',  '.vue', '.json']
  }
};
