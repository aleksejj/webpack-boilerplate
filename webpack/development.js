const webpack = require('webpack');
const merge = require('webpack-merge');
const path = require('path');

const base = require('./base');

module.exports = merge(base, {
  mode: 'development',
  devtool: 'eval-source-map',

  output: {
    filename: 'app.[hash:4].js',
    chunkFilename: '[name].[hash:8].chunk.js',
  },

  module: {
    rules: [{
      test: /(\.scss)$/,
      use: [
        {
          loader: 'style-loader', // creates style nodes from JS strings
        },

        {
          loader: 'css-loader', // translates CSS into CommonJS
          options: {
            sourceMap: true,
            importLoaders: 2
          }
        },

        {
          loader: 'postcss-loader', // post processes CSS & autoprefixes
        },

        {
          // resolves relative paths based on the original source file.
          loader: 'resolve-url-loader',
          options: {
            sourceMap: true,
          }
        },

        {
          loader: 'sass-loader', // compiles SCSS to CSS
          options: {
            sourceMap: true,
          }
        },
      ]
    }]
  },

  plugins: [
    new webpack.DefinePlugin({
      __ENV__: 'development',
    }),
  ],

  watchOptions: {
      poll: 1000,
      ignored: /node_modules/,
  },

  watch: true
});
